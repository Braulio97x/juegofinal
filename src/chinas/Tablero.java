/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chinas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import static java.awt.Cursor.HAND_CURSOR;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author benac
 */
public class Tablero extends JFrame implements ActionListener{
    
    JButton matboton[][]=new JButton[8][8];
    JPanel tab=new JPanel();
    int llave=0;
     int ai=0;
        int aj=0;
        
        int ni=0;
        int nj=0;
    
    Tablero(){
    
        setSize(600,800);
        //setVisible(true);
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        
        tab.setLayout(new GridLayout(8, 8));
        
        for (int i = 0; i < matboton.length; i++) {
            for (int j = 0; j < matboton.length; j++) {
                
                JButton bnuevo=new JButton();
                matboton[i][j]=bnuevo;
                bnuevo.setToolTipText(String.valueOf(i)+","+String.valueOf(j));
                matboton[i][j].addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                         click(bnuevo);
                    }
                });
                matboton[i][j].setCursor(new Cursor(HAND_CURSOR));
                tab.add(matboton[i][j]);
                
            }
            
            tab.validate();
            tab.repaint();
        }
        JButton jugar=new JButton();
        jugar.setText("Jugar");
        jugar.addActionListener(this);
        jugar.setActionCommand("jugar");
     add(tab,BorderLayout.CENTER);
        add(jugar,BorderLayout.PAGE_END);
         
        
         
        
        
        
        
        
    
    }
    
    public void llenar_matriz(){
    
    for (int i = 0; i < matboton.length; i++) {
            for (int j = 0; j < matboton.length; j++) {
                
                
                int punt=i-j;
                if((i==j || punt%2==0 )){
                    
                matboton[i][j].setBackground(new Color(135, 206, 150));
                matboton[i][j].setFont(new java.awt.Font("Calibri", 3, 40)); 
                if(i<=2){
                 matboton[i][j].setText("♣");
                }else if(i>=5){
                    matboton[i][j].setText("♦");
                }
                }else {
                matboton[i][j].setBackground(Color.YELLOW);
                //matboton[i][j].setText("♠");
               
                }
                
               
               
                
            }
            
            tab.validate();
            tab.repaint();
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        
        switch (ae.getActionCommand()){
        
            case "jugar": 
            llenar_matriz();
                break;
        }
        
    }
    
    
    public void mover(){
    
        
    
    
    }
    
    public void click(JButton boton){
        
        if(llave==0){
            ai=Integer.parseInt(boton.getToolTipText().substring(0, 1));
         aj=Integer.parseInt(boton.getToolTipText().substring(2, 3));
            if(!matboton[ai][aj].getText().equals("")){
            llave=1;
            //}else{
            //JOptionPane.showMessageDialog(null, "ESPACIO EN BLANCO");
            }
        
         
        }else{
            llave=0;
         ni=Integer.parseInt(boton.getToolTipText().substring(0, 1));
         nj=Integer.parseInt(boton.getToolTipText().substring(2, 3));
         
         if(matboton[ai][aj].getText().equals("♣")){ 
             
             new Movimientos().abajo(ai, aj, ni, nj,matboton);
             //JOptionPane.showMessageDialog(null, "BAJADA");
         }else if(matboton[ai][aj].getText().equals("♦")){
             new Movimientos().arriba(ai, aj, ni, nj, matboton);
             //JOptionPane.showMessageDialog(null, "SUBIDA");
         }
            System.err.println(llave+"i"+ai);
         
        //System.err.println("Soy el boton en la posicion "+i+","+j);
       
        }
        System.out.println("Hola");
    }
    
    
    
    
}
